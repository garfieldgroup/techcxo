<?php

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>

					<img src="<?php print_custom_field('portrait_image:to_image_src'); ?>" /><br />
					<strong>Position</strong> <?php print_custom_field('position'); ?><br />
					<strong>Email</strong> <a href="mailto:<?php print_custom_field('email'); ?>"><?php print_custom_field('email'); ?></a><br />
					<strong>Phone Number</strong> <?php print_custom_field('phone_number'); ?><br />
					<strong>Address:</strong> <?php print_custom_field('address:do_shortcode'); ?><br />
					<a href="<?php print_custom_field('linkedin_profile'); ?>" target="_blank"><strong>LinkedIn Profile</strong></a><br />
					<strong>Bio</strong> <?php print_custom_field('bio'); ?><br />
					<strong>Sector Experience</strong> <?php print_custom_field('sectorexperience'); ?><br />
					<strong>Practice Focus</strong> <?php print_custom_field('practicefocus'); ?><br />
					<strong>Specialties</strong> <?php print_custom_field('specialties'); ?><br />
					<strong>Education</strong> <?php print_custom_field('education'); ?><br />
					<strong>Thought Capital</strong> <?php print_custom_field('thoughtcapital'); ?><br />

					<h2>FILTER VALUES:</h2>
					<strong>Titles:</strong>
						<?php $items = get_custom_field('titles:to_array');
							foreach ($items as $item) {
								print $item;
								if($item !== end($items)) {
									print ", ";
								}
							}
						?><br />
					<strong>Industry Teams</strong>
						<?php $items = get_custom_field('industry_teams:to_array');
							foreach ($items as $item) {
								print $item;
								if($item !== end($items)) {
									print ", ";
								}
							}
						?>
					<br />
					<strong>Offices</strong>
						<?php $items = get_custom_field('offices:to_array');
							foreach ($items as $item) {
								print $item;
								if($item !== end($items)) {
									print ", ";
								}
							}
						?>
					<br />

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php get_footer(); ?>
